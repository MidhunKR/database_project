import sqlite3 as sq
conn=sq.connect('hdb.db')
c=conn.cursor()
c.execute("""CREATE TABLE Hospital(Hospital_Id int PRIMARY KEY,Hospital_Name text,Bed_Count int)""")
c.execute("""CREATE TABLE Doctor(Doctor_Id int PRIMARY KEY,Doctor_Name text,Hospital_Id int,Joining_Date text,Speciality text,Salary int,Experience text,FOREIGN KEY(Hospital_Id) REFERENCES Hospital(Hospital_Id))""")
def add_doctor():
    print('Enter Doctor ID')
    d_id=int(input())
    print('Enter the name of the Doctor')
    d_name=str(input())
    print('Enter Hospital Id')
    dh_id=int(input())
    print('Enter the joining date in dd-mm-yyyy format')
    
    dj_date=str(input())
    print('Enter speciality')
    d_spl=str(input())
    print('Enter the Salary of Doctor')
    d_sal=int(input())
    print('Enter the Experience Of The Doctor')
    d_exp=str(input())
    c.execute("INSERT INTO Doctor VALUES(?,?,?,?,?,?,?)",(d_id,d_name,dh_id,dj_date,d_spl,d_sal,d_exp))
    conn.commit()
    
def add_hospital():
    print('Entetr the Hospital ID')
    h_id=int(input())
    print('Enter the Hospital Name')
    
    h_name=str(input())
    print('Enter the Bed Count')
    h_cnt=int(input())
    c.execute("INSERT INTO Hospital VALUES(?,?,?)",(h_id,h_name,h_cnt))
    conn.commit()
def find():
    print('Enter the salary')
    sal=int(input())
    print('Enter the Speciality')
    spl=str(input())
    c.execute("SELECT * FROM Doctor WHERE Speciality=? and Salary>?",(spl,sal))
    ar=c.fetchall()
    print('ID'+'|'+'Name'+'|'+'HOSPITAL ID'+'|'+'DOJ'+'|'+'Speciality'+'|'+'SALARY')
    print('---------------------------')
    for i in ar:
        print(str(i[0])+'|'+(i[1])+'|'+str(i[2])+'|'+i[3]+'|'+i[4]+'|'+str(i[5]))
        
def fetch_doc():
    print('Enter the Hospital ID')
    n=int(input())
    c.execute("SELECT Doctor.Doctor_Name,Doctor.Doctor_Id,Doctor.Speciality,Hospital.Hospital_Name FROM Hospital,Doctor ON Hospital.Hospital_Id=Doctor.Hospital_Id AND Hospital.Hospital_Id=?",(n,))
    ar=c.fetchall()
    print('Name'+'|'+'ID'+'|'+'Speciality'+'|'+'HospitalName')
    print('---------------------------')
    for i in ar:
        print(i[0]+'|'+str(i[1])+'|'+i[2]+'|'+i[3])
    
while True:
    print('\n')
    print('1.Add Hospital')
    print('2.Add Doctor')
    print('3.Find Doctors with salary and speciality')
    print('4.Fetch all doctors from Hospital ?')
    print('Enter Option')
    n=int(input())
    if n==1:
        add_hospital()
    if n==2:
        add_doctor()
    if n==3:
        find()
    if n==4:
        fetch_doc()
    if n==5:
        c.execute("SELECT * FROM Doctor")
        i=c.fetchall()
        for j in i:
            print(j)
        
    

import sqlite3 as sq
conn=sq.connect('cardb1.db')
c=conn.cursor()
c.execute("""CREATE TABLE car(carname text,ownname text)""")


def add(cn,on):
    c.execute("INSERT INTO car VALUES(?,?)",(cn,on))
    conn.commit()



for i in range(10):
    print(f'Enter name of car {i+1}')
    cn=str(input())
    print(f'Enter name of owner of the car {i+1}')
    on=str(input())
    add(cn,on)
    
c.execute("SELECT * FROM car")
itr=c.fetchall()
for i in itr:
    print(f'Car name {i[0]},Owner name {i[1]}')
conn.close()
